$(document).ready(function() {
    //     /**
    // * Validates an individual input on form submit
    // * @param {HTMLElement} inputEl
    // * @param {Event} submitEvent
    // */

    const validateNameInput = function(inputEl, submitEvent) {
        const errorEl = inputEl.parentElement.querySelector(".error");
        if (inputEl.value === "" || inputEl.value.length < 3) {
        const LabelEl = inputEl.parentElement.querySelector("label");
        errorEl.innerHTML = `${
            LabelEl.innerText
        } is requered and must be 3 characters or more`;
        inputEl.parentElement.classList.add("invalid");
        submitEvent.preventDefault();
        } else {
        inputEl.parentElement.classList.remove("invalid");
        errorEl.innerHTML = "";
        }
    };

    const validateMessageInput = function(inputEl, submitEvent) {
        const errorEl = inputEl.parentElement.querySelector(".error");
        if (inputEl.value === "" || inputEl.value.length < 10) {
        const LabelEl = inputEl.parentElement.querySelector("label");
        errorEl.innerHTML = `${
            LabelEl.innerText
        } is requered and must be 10 characters or more`;
        inputEl.parentElement.classList.add("invalid");
        submitEvent.preventDefault();
        } else {
        inputEl.parentElement.classList.remove("invalid");
        errorEl.innerHTML = "";
        }
    };

    const validateEmailInput = function(inputEl, submitEvent) {
        const errorEl = inputEl.parentElement.querySelector(".error");
        const regex = /\w+@\w+\.\w+/i;
        if (!inputEl.value.match(regex)) {
        const labelEl = inputEl.parentElement.querySelector("label");
        errorEl.innerHTML = `${labelEl.innerText} is not a valid email address`;
        inputEl.parentElement.classList.add("invalid");
        submitEvent.preventDefault();
        } else {
        inputEl.parentElement.classList.remove("invalid");
        errorEl.innerHTML = "";
        }
    };

    const firstNameEl = document.getElementById("first-name");
    // const messageEl = document.querySelector("textarea");
    const emailEl = document.getElementById("email");
    const messageEl = document.getElementById("message");
    // const contactType = document.getElementById('reason');

    ///////

    let contactTypeValue = document.querySelector("select#reason").value;

    const formEl = document
        .getElementById("connect-form")
        .addEventListener("submit", function(e) {
            validateNameInput(firstNameEl, e);
            validateMessageInput(messageEl, e);
            validateEmailInput(emailEl, e);

            // localStorage.setItem('contact_type', JSON.stringify(contactTypeValue));
            // localStorage.setItem('contact_type',contactTypeValue);

            // $("#reason").change(function() {
                localStorage.setItem("contactType",  $("#reason").val());
            //     $("#reason").val(localStorage.getItem("contactType"));
            // });

            e.preventDefault();
        });

    localStorage.getItem("contact_type");

    // drop down
    $(function() {
        $("#reason").change(function() {
        $(".options").hide();
        $("#" + $(this).val()).show();
        });
    });

    // localStorage.setItem('contact_type','job');

    // localStorage.getItem('contact_type');
    });
