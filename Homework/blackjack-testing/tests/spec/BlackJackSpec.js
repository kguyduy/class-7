
// Unit Test dealerShouldDraw function
// Create some unit tests with Jasmine to test the following cases:

// 10, 9 passed in to function should return false
// Ace, 6 passed in to function should return true
// 10, 7 passed in to function should return false
// 2, 4, 2, 5 passed in should return true

describe('Black Jack Functions', function() {
  describe('Dealer has drawn two cards', function() {
    it('Expect dealerShouldDraw to return', function() {

    const case1 = [
      {
        suite: 'hearts',
        val: 10,
        displayVal: '10'
      },
      {
        suite: 'spades',
        val: 9,
        displayVal: '9'
      }
    ];
    const case2 = [
      {
        suite: 'hearts',
        val: 11,
        displayVal: 'Ace'
      },
      {
        suite: 'spades',
        val: 6,
        displayVal: '6'
      }
    ];
    const case3 = [
      {
        suite: 'hearts',
        val: 10,
        displayVal: '10'
      },
      {
        suite: 'spades',
        val: 7,
        displayVal: '7'
      },
    ];
    const case4 = [
      {
        suite: 'spades',
        val: 2,
        displayVal: '2'
      },
      {
        suite: 'hearts',
        val: 4,
        displayVal: '4'
      },
      {
        suite: 'hearts',
        val: 2,
        displayVal: '2'
      },
      {
        suite: 'hearts',
        val: 5,
        displayVal: '5'
      },
  ];
    expect(dealerShouldDraw(case1)).toBe(false);
    expect(dealerShouldDraw(case2)).toBe(true);
    expect(dealerShouldDraw(case3)).toBe(false);
    expect(dealerShouldDraw(case4)).toBe(true);
  })
})
})
