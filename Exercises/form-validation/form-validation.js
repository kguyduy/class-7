// $(document).ready(function() {
//     /**
// * Validates an individual input on form submit
// * @param {HTMLElement} inputEl
// * @param {Event} submitEvent
// */
const validateTextInput = function(inputEl, submitEvent) {
    const errorEl = inputEl.parentElement.querySelector('.error');
    if (inputEl.value === '' || inputEl.value.length < 3) {
        const LabelEl = inputEl.parentElement.querySelector('label');
        errorEl.innerHTML = `${LabelEl.innerText} is requered and must be 3 characters or more`;
        inputEl.parentElement.classList.add('invalid');
        submitEvent.preventDefault();
    } else {
        inputEl.parentElement.classList.remove('invalid');
        errorEl.innerHTML = '';
    }
}

const validateEmailInput = function(inputEl, submitEvent) {
    const errorEl = inputEl.parentElement.querySelector('.error');
    const regex = /\w+@\w+\.\w+/i;
    if (!inputEl.value.match(regex)) {
        const labelEl = inputEl.parentElement.querySelector('label');
        errorEl.innerHTML = `${labelEl.innerText} is not a valid email address`;
        inputEl.parentElement.classList.add('invalid');
        submitEvent.preventDefault();
    } else {
        inputEl.parentElement.classList.remove('invalid');
        errorEl.innerHTML = '';
    }
}

    const firstNameEl = document.getElementById('first-name');
    const lastNameEl = document.getElementById('last-name');
    const emailEl = document.getElementById('email');

    const formEl = document.getElementById('connect-form').addEventListener('submit', function(e) {
            validateTextInput(firstNameEl, e);
            validateTextInput(lastNameEl, e);
            validateEmailInput(emailEl, e);



        e.preventDefault();
        });

// });

