// Kept the starter Jasmine Player Spec Code
// Update this!
// Try a few different cases:
// – 10, 7 passed in (remember that you have to pass in full card
// objects)
// – Ace, 9 passed in
// – 10, 6, Ace passed in

describe('calcPoints Function', function() {
  it('should correctly calculate the points', function(){
    const case1 = [
      {
        suite: 'hearts',
        val: 10,
        displayVal: '10'
      },
      {
        suite: 'spades',
        val: 7,
        displayVal: '7'
      }
    ];
    const case2 = [
      {
        suite: 'hearts',
        val: 11,
        displayVal: 'Ace'
      },
      {
        suite: 'spades',
        val: 9,
        displayVal: '9'
      }
    ];
    const case3 = [
      {
        suite: 'hearts',
        val: 10,
        displayVal: '10'
      },
      {
        suite: 'spades',
        val: 6,
        displayVal: '6'
      },
      {
        suite: 'hearts',
        val: 11,
        displayVal: 'Ace'
      },
    ];
    expect(calcPoints(case1).total).toEqual(17);
    expect(calcPoints(case2).total).toEqual(20);
    expect(calcPoints(case3).total).toEqual(17);
  });
});
